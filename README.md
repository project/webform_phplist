CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

 This module adds a handler for sending webform submissions to phpList.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/webform_phplist

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/webform_phplist

REQUIREMENTS
------------

This module requires the following modules:

 * Webform (https://www.drupal.org/project/webform)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/extending-drupal/installing-drupal-modules
   for further information.

CONFIGURATION
-------------

 * Add and configure a webform
 * Add "Post to phpList" handler under Settings -> E-mail/Handlers
 * Configure the handler. You have these options:
   - Title, Administrative notes: only visible to webform admins.
   - Add submission to these lists
   - Mailing list opt-in:
     • Double opt-in: Send a confirmation email to verify the email address.
       The email sender, reply-to, subject and body are taken from the phpList
       configuration.
     • Single opt-in: Set the “confirmed” option in phpList to 1.
