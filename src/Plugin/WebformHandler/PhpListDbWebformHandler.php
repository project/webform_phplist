<?php

namespace Drupal\webform_phplist\Plugin\WebformHandler;

use Drupal;
use Drupal\Component\Uuid\Php;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\ConnectionNotDefinedException;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\DatabaseException;
use Drupal\Core\DrupalKernel;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Exception;
use PDO;
use PDOException;

/**
 * Sends a webform submission to phpList.
 *
 * @WebformHandler(
 *   id = "phplist_db",
 *   label = @Translation("Add subscriber to phpList"),
 *   category = @Translation("External"),
 *   description = @Translation("Sends data from a webform submission to configurable phpList lists."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class PhpListDbWebformHandler extends WebformHandlerBase {

  private int $uid;

  private string $uniqid;

  private int $confirmed;

  private string $email;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'lists' => [1],
      'htmlemail' => TRUE,
      'double_opt_in' => 1,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    // phpList settings.
    $form['phplist_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('phpList settings'),
      '#open' => TRUE,
    ];

    try {
      $db = Database::getConnection('default', 'phplist');
      $lists = $this->getLists($db);
    }
    catch (DatabaseException|PDOException $e) {
      // This gets thrown if the array key in settings.php exists, but is wrong.
      $error_message = $e->getMessage() . '<br>';
      $error_message .= $this->t('Please make sure the code block %code in :file is correct.',
        $this->getDatabaseHelp());
      return $this->showError($error_message);
    }
    catch (ConnectionNotDefinedException) {
      // This gets thrown if the array key in settings.php does not exist.
      $error_message = $this->t('Please add the code %code to the file :file.',
        $this->getDatabaseHelp());
      $error_message .= '<br><br>' . $this->t('The structure of the array should match the :code lines.',
          [':code' => '`$databases["default"]["default"]']);
      $error_message .= ' ' . $this->t('You need to have the MySQL / MariaDB database of phpList accessible to Drupal.');
      return $this->showError($error_message);
    }

    $elements = $this->webform->getElementsDecodedAndFlattened();
    $titles = array_map(fn($element) => $element['#title'] ?? '', $elements);
    $select = [
      '#type' => 'select',
      '#options' => [0 => t('Ignore')] + array_filter($titles),
    ];
    $attributes = $db->select('phplist_user_attribute', "a")
      ->fields('a', ['id', 'name'])
      ->execute()
      ->fetchAllKeyed();
    foreach ($attributes as $id => $name) {
      $str = 'attribute' . $id;
      $form['phplist_settings'][$str] = [
          '#title' => $name,
          '#default_value' => $this->configuration[$str] ?? 0,
        ] + $select;
    }

    $form['phplist_settings']['lists'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Add submission to these lists'),
      '#description' => $this->t('Select lists that webform submitters should be subscribed to in phpList.'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['lists'] ?: array_keys($lists),
      '#options' => $lists,
    ];
    $form['phplist_settings']['htmlemail'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send HTML emails'),
      '#default_value' => $this->configuration['htmlemail'] ?? TRUE,
    ];
    $adminUrl = strtr($this->getConfig($db, ['confirmationurl'])['confirmationurl'],
      ['?p=confirm' => 'admin/?page=configure']);
    $form['phplist_settings']['double_opt_in'] = [
      '#type' => 'radios',
      '#title' => $this->t('Mailing list opt-in'),
      '#default_value' => $this->configuration['double_opt_in'] ?? 1,
      '#options' => [
        1 => $this->t('Double opt-in') . ': ' .
          $this->t('Send a confirmation email to verify the email address.') . ' ' .
          $this->t('The email sender, reply-to, subject and body are taken from the <a href=":url">phpList configuration</a>.', [":url" => $adminUrl]),
        0 => $this->t('Single opt-in') . ': ' .
          $this->t('Set the “confirmed” option in phpList to 1.'),
      ],
    ];

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValues();
    if (isset($values['phplist_settings'])) {
      $values += $values['phplist_settings'];
      unset($values['phplist_settings']);
    }
    foreach ($values as $key => $value) {
      $this->configuration[$key] = $value;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = FALSE): void {
    if ($update) {
      return;
    }
    $webform = $webform_submission->getWebform();
    $elements = $webform->getElementsDecodedAndFlattened();
    $messenger = Drupal::messenger();
    foreach ($elements as $key => $element) {
      if (in_array($element['#type'], ['email', 'webform_email_confirm'])) {
        if (isset($mail_element)) {
          $messenger->addWarning($this->t('This form contains multiple email elements.'));
        }
        $mail_element = $key;
      }
    }
    if (!isset($mail_element)) {
      $messenger->addError($this->t('For the %label handler to work, please add an email element to this form.', ['%label' => 'frog']));
      return;
    }

    $this->email = $webform_submission->getElementData($mail_element);
    $db = Database::getConnection('default', 'phplist');
    $query = $db->select('phplist_user_user', 'u')
      ->fields('u', ['id', 'uniqid', 'confirmed'])
      ->where('LOWER(u.email) = LOWER(:email)', [':email' => $this->email]);
    $results = $query->execute();
    $fields = $results->fetchAssoc();
    if ($fields === FALSE) {
      $fields = $this->addUser($db, ['email' => $this->email]);
      $this->addUserHistory($db, $fields['id']);
      $this->addUserAttributes($db, $fields['id'], $webform_submission);
    }
    else {
      $this->uid = $fields['id'];
      $this->uniqid = $fields['uniqid'];
      $this->confirmed = $fields['confirmed'];
    }
    foreach ($this->configuration['lists'] as $listid => $selected) {
      if (!$selected) {
        continue;
      }
      $insert = $db->insert('phplist_listuser');
      $now = date('Y-m-d H:i:s');
      $insert->fields([
        'userid' => $fields['id'],
        'listid' => $listid,
        'entered' => $now,
      ]);
      $list_name = $db->select('phplist_list', 'phplist_list')
        ->fields('phplist_list', ['name'])
        ->condition('id', $listid)
        ->execute()
        ->fetchField();
      if (!$list_name) {
        $form = $this->getWebform();
        Drupal::logger('webform_phplist')
          ->error('List @list not found; Please fix the @id handler configuration of webform @form. Saving once can help.', [
            '@list' => $listid,
            '@id' => $this->handler_id,
            '@form' => $form->id(),
            'link' => $form->toLink($this->t('Fix handler configuration'), 'handlers')
              ->toString(),
          ]);
        $messenger->addError(
          t('There was a problem subscribing your email address.') . ' ' .
          t('This is not your fault.') . ' ' .
          t('Please let the the site owner know to check under Administration » Reports » Recent log messages for more details.'));
        continue;
      }
      try {
        $insert->execute();
        $messenger->addStatus(t('The email address %email was subscribed successfully to %list.', [
          '%email' => $this->email,
          '%list' => ucfirst($list_name),
        ]));
      }
      catch (Exception) {
        // This is okay: the user is already subscribed
      }
      if (!$this->sendConfirmationEmail($db)) {
        Drupal::messenger()
          ->addError(t('Sending of confirmation e-mail to %mail failed.', ["%mail" => $this->email]));
      }
    }
  }

  private function addUser(Connection $db, array $assoc): array {
    $insert = $db->insert('phplist_user_user');
    $double_opt_in = $this->configuration['double_opt_in'] ?? 1;
    $extra = [
      'uniqid' => $this->uniqid = bin2hex(random_bytes(16)),
      'uuid' => (new Php())->generate(),
      'confirmed' => $this->confirmed = (1 - $double_opt_in),
      'optedin' => 1,
      'entered' => date('Y-m-d H:i:s'),
      'htmlemail' => $this->configuration['htmlemail'] ?? TRUE,
    ];
    $insert->fields($assoc + $extra);
    return [
      'id' => $this->uid = $insert->execute(),
    ];
  }

  private function addUserHistory(Connection $db, int $user_id): void {
    $detail = t('Via Drupal webform %label', ['%label' => $this->webform->label()]);
    $history = ['detail' => $detail];
    $sys_info = '';
    $sys_arrays = array_merge($_ENV, $_SERVER);
    $default = [
      'HTTP_USER_AGENT',
      'HTTP_REFERER',
      'REMOTE_ADDR',
      'REQUEST_URI',
      'HTTP_X_FORWARDED_FOR',
    ];
    foreach ($sys_arrays as $key => $val) {
      if (in_array($key, $default)) {
        $sys_info .= "\n" . strip_tags($key) . ' = ' . htmlspecialchars($val);
      }
    }

    $insert = $db->insert('phplist_user_user_history');
    $insert->fields([
        'systeminfo' => $sys_info,
        'userid' => $user_id,
        'date' => date('Y-m-d H:i:s'),
        'summary' => 'Subscription',
        'ip' => Drupal::request()->getClientIp(),
      ] + $history);
    $insert->execute();
  }

  protected function addUserAttributes(Connection $db, int $user_id, WebformSubmissionInterface $webform_submission): void {
    foreach ($this->configuration as $name => $value) {
      if (str_starts_with($name, 'attribute')) {
        $insert = $db->insert('phplist_user_user_attribute');
        $insert->fields([
          'attributeid' => substr($name, 9),
          'userid' => $user_id,
          'value' => $webform_submission->getElementData($value),
        ]);
        $insert->execute();
      }
    }
  }

  /**
   * @return array
   */
  protected function getDatabaseHelp(): array {
    $site_path = DrupalKernel::findSitePath(Drupal::request(), FALSE);
    return [
      '%code' => '$databases["phplist"]["default"] = [ ... ];',
      ':file' => getcwd() . '/' . $site_path . '/settings.php',
    ];
  }

  /**
   * @param string $error_message
   *
   * @return string[]
   */
  protected function showError(string $error_message): array {
    Drupal::messenger()
      ->addError(Markup::create($error_message));
    return ['#markup' => '<div class="messages--error messages"><p class="error">' . $error_message . '</p></div>'];
  }

  private function sendConfirmationEmail(Connection $db): bool {
    if ($this->confirmed) {
      return TRUE;
    }
    $fields = $this->getConfig($db, [
      'subscribesubject',
      'subscribemessage',
      'message_from_address',
      'message_replyto_address',
      'confirmationurl',
    ]);

    $msg = strtr($fields['subscribemessage'],
      [
        '[CONFIRMATIONURL]' => $fields['confirmationurl'] . '&uid=' . $this->uniqid,
        '[LISTS]' => join("\n", $this->getLists($db)),
      ]);
    $params = [
      'subject' => $fields['subscribesubject'],
      'body' => $msg,
      'from' => $fields['message_from_address'],
    ];
    $langcode = Drupal::currentUser()->getPreferredLangcode();

    /** @var \Drupal\Core\Mail\MailManager $mailService */
    $mailService = Drupal::service('plugin.manager.mail');
    $result = $mailService->mail(
      "webform_phplist", "webform_phplist_confirmation",
      $this->email, $langcode, $params, $fields['message_replyto_address']);

    // Check if the email was sent successfully.
    return $result['result'];
  }

  /**
   * @param \Drupal\Core\Database\Connection $db
   *
   * @return array<int,string>
   * @throws ConnectionNotDefinedException|DatabaseException|PDOException|\Exception
   */
  private function getLists(Connection $db): array {
    $query = $db->select('phplist_list', "l")
      ->fields('l', ['id', 'name', 'description']);
    $attributes = $query->execute();

    $lists = [];
    while ($list = $attributes->fetch(PDO::FETCH_ASSOC, 0)) {
      $lists[$list['id']] = $list['name'] . ' - ' . $list['description'];
    }
    return $lists;
  }

  /**
   * @param \Drupal\Core\Database\Connection $db
   * @param list<string> $items
   *
   * @return list<string>
   */
  private function getConfig(Connection $db, array $items): array {
    return $db->select('phplist_config', 'config')
      ->fields('config', ['item', 'value'])
      ->condition('item', $items, 'IN')
      ->execute()
      ->fetchAllKeyed();
  }

}
